resource "aws_instance" "webserver" {
  ami           = "ami-0dfcb1ef8550277af"
  instance_type = "t3.nano"
  tags = {
    Name = "${terraform.workspace}-instance"
  }
}

